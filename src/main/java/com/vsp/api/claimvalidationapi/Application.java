package com.vsp.api.claimvalidationapi;

import com.vsp.il.util.Preferences;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableHystrix
public class Application {

    public static void main(String[] args) {
        Preferences.initialize();
        SpringApplication.run(Application.class, args);
    }

}